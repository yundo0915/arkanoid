#pragma strict

var hit = 1;
var sound : AudioClip;
var explosion : Transform;

function Start () {

}

function Update () {

}

function SetCollision(flag : boolean){
	AudioSource.PlayClipAtPoint(sound, transform.position);
	
	if(jsGameManager.state != STATE.DEMO){
		hit--;
	}
	if(flag == true){
		hit = -1;
	}
	
	if(hit >= 0){
		jsGameManager.state = STATE.HIT;
		
		var color = transform.renderer.material.color;
		transform.renderer.material.color = Color.red;
		
		yield WaitForSeconds(0.2);
		transform.renderer.material.color = color;
	}
	else
	{
		Instantiate(explosion, transform.position, Quaternion.identity);
		
		jsGameManager.state = STATE.DESTROY;
		jsGameManager.blockNum = int.Parse(transform.tag.Substring(5, 1));
		jsGameManager.blockPos = transform.position;
		Destroy(gameObject);
	}
}